﻿using QuandParie.Core.Domain;
using QuandParie.Core.Services;
using QuandParie.Core.Persistance;
using System.Threading.Tasks;
using QuandParie.Core.ReadOnlyInterfaces;

namespace QuandParie.Core.Application
{
    public class RegistrationApplication
    {
        
        private readonly ICustomerRepository customer;

        private readonly IDocumentRepository document;

        private readonly IIdentityProofer identityProofer;

        private readonly IAddressProofer addressProofer;

        public RegistrationApplication(
            ICustomerRepository customer, 
            IDocumentRepository document, 
            IIdentityProofer identityProofer,
            IAddressProofer addressProofer)
        {
            this.customer = customer;
            this.document = document;
            this.identityProofer = identityProofer;
            this.addressProofer = addressProofer;
        }

        public async Task<IReadOnlyCustomer> CreateAccount(string email, string firstName, string lastName)
        {
            var new_customer = new Customer(email, firstName, lastName);
            await customer.SaveAsync(new_customer);

            return new_customer;
        }

        public async Task<IReadOnlyCustomer> GetAccount(string email)
        {
            return await customer.GetAsync(email);
        }

        public async Task<bool> UploadIdentityProof(string email, byte[] identity_proof)
        {
            var new_customer = await customer.GetAsync(email);
            if (!identityProofer.Validates(new_customer, identity_proof))
                return false;

            await document.SaveAsync(DocumentType.IdentityProof, email, identity_proof);

            new_customer.IsIdentityVerified = true;
            await customer.SaveAsync(new_customer);

            return true;
        }

        public async Task<bool> UploadAddressProof(string email, byte[] adress_proof)
        {
            var new_customer = await this.customer.GetAsync(email);
            if (!addressProofer.Validates(new_customer, out var adress, adress_proof))
                return false;

            await document.SaveAsync(DocumentType.AddressProof, email, adress_proof);

            new_customer.Address = adress;
            await this.customer.SaveAsync(new_customer);

            return true;
        }
    }
}
